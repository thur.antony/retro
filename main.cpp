#include "retro.hpp"

namespace	w
{
	int		width;
	int		height;
	int		gameLoop = 1;
	int		i;
	int		i1;
}

namespace	obj
{
	I		h;
	Bullet	b[150];
	//Enemy	e[150];
}

namespace	t
{
	clock_t	t0;
}

void		controls(int key)
{
	switch(key)
	{
		case KEY_LEFT:
			obj::h.setX(-1);
			obj::h.display();
			break ;
		case KEY_RIGHT:
			obj::h.setX(1);
			obj::h.display();
			break ;
		case KEY_UP:
			obj::h.setY(-1);
			obj::h.display();
			break ;
		case KEY_DOWN:
			obj::h.setY(1);
			obj::h.display();
			break ;
		case 27:
			w::gameLoop = 0;
			break ;
		case 32:
			w::i = ++w::i % 150;
			obj::b[w::i] = Bullet(obj::h.getX(), obj::h.getY());
			break ;
		}
}

void		display_health(void)
{
	int		i;
	int		hp = obj::h.getHp();

	i = -1;
	mvprintw(w::height - 1,0, "health:");
	start_color();
	init_pair(1, COLOR_RED, COLOR_BLACK);
	attron(COLOR_PAIR(1));
	while (++i < hp)
		mvaddch(w::height - 1, 8 + i * 2, ACS_DIAMOND);
	attroff(COLOR_PAIR(1));
}

int			main(void)
{
	initscr();
	getmaxyx(stdscr, w::height, w::width);
	noecho();
	curs_set(0);
	keypad(stdscr, true);
	cbreak();
	nodelay(stdscr, true);

	w::i = -1;
	while (w::gameLoop)
	{
		t::t0 = clock();
		refresh();
		w::i1 = 0;
		while (w::i1 <= w::i && w::i1 < 150)
		{
			obj::b[w::i1].update();
			w::i1++;
		}
		obj::h.display();
		display_health();
		controls(getch());
		while (clock() - t::t0 <  20000);	
	}
	endwin();
	return 0;
}

