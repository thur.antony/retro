NAME = ft_retro

SRC_PROJECT = ./Bullet.cpp \
			./I.cpp \
			./Entry.cpp \
			./main.cpp

FLAGS = -Wall -Wextra -Werror -I. -lcurses

all: $(NAME)

$(NAME): 
	@clang++ -o $(NAME) $(FLAGS) $(SRC_PROJECT)

%.o: %.c
	@clang++ $(FLAGS) -o $@ -c $<

clean:
	@/bin/rm -f $(OBJECT_PROJECT)

fclean: clean
	@/bin/rm -f $(NAME)

re:	fclean all
